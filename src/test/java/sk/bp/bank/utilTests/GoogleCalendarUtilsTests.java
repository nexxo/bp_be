package sk.bp.bank.utilTests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sk.bp.bank.util.GoogleCalendarUtils;

import java.util.Calendar;
import java.util.TimeZone;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GoogleCalendarUtilsTests {

    @Value("${timezone}")
    private String timeZone;

    @Autowired
    private GoogleCalendarUtils googleCalendarUtils;

    @Test
    public void givenDate_whenDateIsWeekdayAndNotPublicHoliday_thenReturnTrue() {
        final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
        calendar.set(2019, Calendar.DECEMBER, 16);

        Assert.assertEquals(calendar.get(Calendar.DAY_OF_WEEK), Calendar.MONDAY);
        Assert.assertTrue(googleCalendarUtils.isBankingDay(calendar.getTime()));
    }

    @Test
    public void givenDate_whenDateIsWeekend_thenReturnFalse() {
        final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(timeZone));

        calendar.set(2019, Calendar.DECEMBER, 15);
        Assert.assertEquals(calendar.get(Calendar.DAY_OF_WEEK), Calendar.SUNDAY);
        Assert.assertFalse(googleCalendarUtils.isBankingDay(calendar.getTime()));

        calendar.set(2019, Calendar.DECEMBER, 14);
        Assert.assertEquals(calendar.get(Calendar.DAY_OF_WEEK), Calendar.SATURDAY);
        Assert.assertFalse(googleCalendarUtils.isBankingDay(calendar.getTime()));
    }

    @Test
    public void givenDate_whenDateIsPublicHoliday_thenReturnFalse() {
        final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(timeZone));

        calendar.set(2019, Calendar.DECEMBER, 24);
        Assert.assertFalse(googleCalendarUtils.isBankingDay(calendar.getTime()));

        calendar.set(2019, Calendar.DECEMBER, 25);
        Assert.assertFalse(googleCalendarUtils.isBankingDay(calendar.getTime()));

        calendar.set(2020, Calendar.JANUARY, 6);
        Assert.assertFalse(googleCalendarUtils.isBankingDay(calendar.getTime()));
    }
}
