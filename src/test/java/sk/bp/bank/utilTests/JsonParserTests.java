package sk.bp.bank.utilTests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sk.bp.bank.util.JsonParser;
import sk.bp.bank.util.dto.GoogleCalendarDto;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JsonParserTests {

    private JsonParser<GoogleCalendarDto> jsonParser;

    @Before
    public void JsonParserInit() {
        this.jsonParser = new JsonParser<>();
    }

    @Test
    public void givenJsonParser_whenParse_thenReturnDtoWithHolidayInfo() {
        try {
            final String jsonFileName = "public_holidays.json";
            final GoogleCalendarDto parsedResult = jsonParser.parse(jsonFileName, GoogleCalendarDto.class);

            Assert.assertNotNull(parsedResult);
            Assert.assertFalse(parsedResult.getItems().isEmpty());
        } catch (IOException e) {
            Assert.fail();
        }
    }
}
