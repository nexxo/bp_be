package sk.bp.bank.utilTests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sk.bp.bank.account.entity.Account;
import sk.bp.bank.overlimittransaction.Amount;
import sk.bp.bank.overlimittransaction.constants.OrderCategory;
import sk.bp.bank.overlimittransaction.constants.State;
import sk.bp.bank.overlimittransaction.entity.ReportedOverLimitTransaction;
import sk.bp.bank.util.TransactionValidator;
import sk.bp.bank.vaultsupply.Type;
import sk.bp.bank.vaultsupply.entity.VaultSupply;

import java.util.*;

import static sk.bp.bank.overlimittransaction.constants.ErrorCode.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionValidatorTests {

    @Autowired
    private TransactionValidator txValidator;
    private ReportedOverLimitTransaction transaction;

    @Before
    public void setTransaction() {
        final String timeZone = "Europe/Bratislava";
        final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
        final Account sourceAccount = new Account(
                null, "invalid", "ucet",
                0.1, 200.0, "name", "note"
        );

        final Amount amount = new Amount(2000, 2, "GBP");
        final VaultSupply vaultSupply = new VaultSupply(null, 200.0, 10, Type.BANKNOTE);
        final List<VaultSupply> vaultSupplies = Collections.singletonList(vaultSupply);

        calendar.set(2055, Calendar.DECEMBER, 24);

        this.transaction = new ReportedOverLimitTransaction(
                null, OrderCategory.DOMESTIC, State.CLOSED, sourceAccount, "1234",
                "asdsad123", amount, vaultSupplies, new Date(), calendar.getTime(), "note",
                "org1", "Me"
        );
    }

    @Test
    public void givenTransaction_whenValidated_thenReturnListOfDiscrepancies() {
        // some discrepancies were collected
        Assert.assertFalse(txValidator.validateTransaction(transaction).isEmpty());
    }

    @Test
    public void givenTransaction_whenValidated_thenMatchReturnedDiscrepancies() {
        final List<String> collectedDiscrepancies = txValidator.validateTransaction(transaction);

        Assert.assertEquals(5, collectedDiscrepancies.size());
        Assert.assertTrue(collectedDiscrepancies.contains(INVALID_DATE));
        Assert.assertTrue(collectedDiscrepancies.contains(IBAN_INVALID));
        Assert.assertTrue(collectedDiscrepancies.contains(LIMIT_EXCEEDED));
        Assert.assertTrue(collectedDiscrepancies.contains(CURRENCY_NOT_AVAILABLE));
        Assert.assertTrue(collectedDiscrepancies.contains(CATEGORY_INVALID));
    }
}
