package sk.bp.bank.controllerTests;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;


@WebFluxTest
@RunWith(SpringRunner.class)
public abstract class AbstractControllerTest {

    private WebTestClient client;

    @Autowired
    private ApplicationContext applicationContext;

    @Before
    public void clientSetup() {
        this.client = WebTestClient
                .bindToApplicationContext(applicationContext)
                .configureClient()
                .build();
    }

    public WebTestClient getWebClient() {
        return client;
    }
}

