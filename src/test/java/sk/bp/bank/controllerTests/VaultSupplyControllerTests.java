package sk.bp.bank.controllerTests;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import reactor.core.publisher.Flux;
import sk.bp.bank.vaultsupply.Type;
import sk.bp.bank.vaultsupply.constants.Path;
import sk.bp.bank.vaultsupply.controller.VaultSuppliesController;
import sk.bp.bank.vaultsupply.entity.VaultSupply;
import sk.bp.bank.vaultsupply.service.VaultSuppliesService;

import java.util.Arrays;
import java.util.List;


@WebFluxTest(controllers = VaultSuppliesController.class)
public class VaultSupplyControllerTests extends AbstractControllerTest {

    @MockBean
    private VaultSuppliesService vaultSuppliesService;

    @Test
    @WithMockUser
    public void givenRequest_whenValidRequest_thenReturnVaultSupplies() {
        final VaultSupply eur200 = new VaultSupply(null, 200.0, 1, Type.BANKNOTE);
        final VaultSupply eur40 = new VaultSupply(null, 20.0, 2, Type.BANKNOTE);
        final List<VaultSupply> vaultSupplies = Arrays.asList(eur200, eur40);

        Mockito.when(vaultSuppliesService.getAllVaultSupplies()).thenReturn(Flux.fromIterable(vaultSupplies));

        super.getWebClient()
                .get()
                .uri(Path.ROOT)
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBodyList(VaultSupply.class)
                .value(vaultSupplyResponse -> Assert.assertEquals(vaultSupplies, vaultSupplyResponse));
    }
}
