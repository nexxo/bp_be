package sk.bp.bank.controllerTests;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import reactor.core.publisher.Mono;
import sk.bp.bank.client.constants.Path;
import sk.bp.bank.client.controller.ClientController;
import sk.bp.bank.client.dto.ClientDto;
import sk.bp.bank.client.entity.Client;
import sk.bp.bank.client.service.ClientService;

import java.util.ArrayList;
import java.util.Date;

@WebFluxTest(controllers = ClientController.class)
public class ClientControllerTests extends AbstractControllerTest {

    @MockBean
    private ClientService clientService;

    @Test
    @WithMockUser
    public void givenIdentificationId_whenGivenRightId_thenSecuredRestShouldReturnClient() {
        final String identificationId = "1234";
        final Client client = new Client("id", "Peter", "Hajduk", new Date(), "1234", new ArrayList<>());
        final ClientDto clientDto = new ClientDto(client);

        Mockito.when(clientService.getClientByIdentificationId(identificationId)).thenReturn(Mono.just(client));

        super.getWebClient()
                .get()
                .uri(Path.ROOT + "?identificationId=" + identificationId)
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody(ClientDto.class)
                .value(actualResponse -> Assert.assertEquals(clientDto, actualResponse));
    }

}
