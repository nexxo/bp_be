package sk.bp.bank.controllerTests;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import reactor.core.publisher.Flux;
import sk.bp.bank.currency.constants.Path;
import sk.bp.bank.currency.controller.CurrencyController;
import sk.bp.bank.currency.entity.Currency;
import sk.bp.bank.currency.service.CurrencyService;

import java.util.Arrays;
import java.util.List;

@WebFluxTest(controllers = CurrencyController.class)
public class CurrencyControllerTests extends AbstractControllerTest {

    @MockBean
    private CurrencyService currencyService;

    @Test
    @WithMockUser
    public void givenRequest_whenRequestIsValid_thenRetrieveCurrenciesList() {
        final Currency eur = new Currency("a1", "EUR", "name", 2, "EUR", true);
        final Currency gbp = new Currency("a2", "GBP", "name", 2, "GBP", true);
        final List<Currency> currenciesList = Arrays.asList(eur, gbp);

        Mockito.when(currencyService.getAllAvailableCurrencies()).thenReturn(Flux.fromIterable(currenciesList));

        super.getWebClient()
                .get()
                .uri(Path.ROOT)
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBodyList(Currency.class)
                .value(currenciesResponse -> Assert.assertEquals(currenciesList, currenciesResponse));
    }
}
