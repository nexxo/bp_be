package sk.bp.bank.controllerTests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;
import sk.bp.bank.account.entity.Account;
import sk.bp.bank.overlimittransaction.Amount;
import sk.bp.bank.overlimittransaction.constants.OrderCategory;
import sk.bp.bank.overlimittransaction.constants.Path;
import sk.bp.bank.overlimittransaction.constants.State;
import sk.bp.bank.overlimittransaction.controller.ReportedOverLimitTransactionController;
import sk.bp.bank.overlimittransaction.entity.ReportedOverLimitTransaction;
import sk.bp.bank.overlimittransaction.service.ReportedOverLimitTransactionService;
import sk.bp.bank.util.TransactionValidator;
import sk.bp.bank.vaultsupply.Type;
import sk.bp.bank.vaultsupply.entity.VaultSupply;

import java.util.*;

import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;
import static sk.bp.bank.overlimittransaction.constants.ErrorCode.ID_NOT_FOUND;

@WebFluxTest(controllers = ReportedOverLimitTransactionController.class)
public class ReportedOverLimitTransactionTests extends AbstractControllerTest {

    @MockBean
    private ReportedOverLimitTransactionService transactionService;

    @MockBean
    private TransactionValidator transactionValidator;

    private ReportedOverLimitTransaction transaction;

    @Before
    public void setup() {
        final String timeZone = "Europe/Bratislava";
        final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
        final Account account = new Account(
                UUID.randomUUID().toString(), null, "prod", 200.0,
                2000.0, "name", "note"
        );
        final Amount amount = new Amount(200, 2, "EUR");
        final VaultSupply vaultSupply = new VaultSupply(
                UUID.randomUUID().toString(), 200.0, 2, Type.BANKNOTE
        );
        final List<VaultSupply> vaultSupplies = Collections.singletonList(vaultSupply);

        calendar.set(2050, Calendar.DECEMBER, 18);

        this.transaction = new ReportedOverLimitTransaction(
                UUID.randomUUID().toString(), OrderCategory.DOMESTIC, State.CLOSED, account,
                UUID.randomUUID().toString(), "123A", amount, vaultSupplies, new Date(),
                calendar.getTime(), "note", UUID.randomUUID().toString(), "Peter"
        );

        Mockito.when(transactionValidator.validateTransaction(transaction)).thenReturn(Collections.emptyList());
        Mockito.when(transactionService.getOneTransaction(transaction.getId())).thenReturn(Mono.just(transaction));
    }

    @Test
    @WithMockUser
    public void givenTransactionId_whenTransactionExists_thenReturnTransactionWithStatus200() {
        super.getWebClient()
                .get()
                .uri(Path.ROOT + "/" + this.transaction.getId())
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody(ReportedOverLimitTransaction.class)
                .value(transactionResponse -> Assert.assertEquals(this.transaction, transactionResponse));
    }

    @Test
    @WithMockUser
    public void givenInvalidTransactionId_whenTransactionIdIsInvalid_thenExitWithStatus400() {
        Mockito.when(transactionService.getOneTransaction(transaction.getId())).thenReturn(Mono.empty());

        super.getWebClient()
                .get()
                .uri(Path.ROOT + "/" + this.transaction.getId())
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody(String.class)
                .value(transactionResponse -> Assert.assertTrue(transactionResponse.contains(ID_NOT_FOUND)));
    }

    @Test
    @WithMockUser
    public void givenValidTransaction_whenTransactionPosted_thenCreateTransactionWithStatus200() {
        final Mono<ReportedOverLimitTransaction> serviceResponse = Mono.just(transaction);

        Mockito.when(transactionService.createTransaction(transaction)).thenReturn(serviceResponse);

        super.getWebClient()
                .mutateWith(csrf())
                .post()
                .uri(Path.ROOT)
                .body(BodyInserters.fromObject(transaction))
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody(ReportedOverLimitTransaction.class)
                .value(transactionResponse -> Assert.assertEquals(this.transaction, transactionResponse));
    }

    @Test
    @WithMockUser
    public void givenTransactionId_whenTransactionExists_thenDeleteTransactionWithStatus200() {
        final Mono<Void> serviceResponse = Mono.empty();

        Mockito.when(transactionService.deleteTransactionById(this.transaction.getId())).thenReturn(serviceResponse);

        super.getWebClient()
                .mutateWith(csrf())
                .delete()
                .uri(Path.ROOT + "/" + this.transaction.getId())
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader();
    }
}
