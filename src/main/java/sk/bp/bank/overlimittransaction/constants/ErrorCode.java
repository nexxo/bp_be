package sk.bp.bank.overlimittransaction.constants;

public class ErrorCode {
    public static final String ID_NOT_FOUND = "ID_NOT_FOUND";
    public static final String FIELD_INVALID = "FIELD_INVALID";
    public static final String CURRENCY_NOT_AVAILABLE = "CURRENCY_NOT_AVAILABLE";
    public static final String FIELD_INVALID_TOO_NEAR_IN_FUTURE = "FIELD_INVALID_TOO_NEAR_IN_FUTURE";
    public static final String INVALID_DATE_IN_PAST = "INVALID_DATE_IN_PAST";
    public static final String INVALID_DATE = "INVALID_DATE";
    public static final String ACCOUNT_OFFLINE = "ACCOUNT_OFFLINE";
    public static final String CATEGORY_INVALID = "CATEGORY_INVALID";
    public static final String IBAN_INVALID = "IBAN_INVALID";
    public static final String LIMIT_EXCEEDED = "LIMIT_EXCEEDED";


    // disable instantiation of this class as it is only for error constants
    private ErrorCode() {
    }
}
