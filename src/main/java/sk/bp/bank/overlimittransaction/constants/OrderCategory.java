package sk.bp.bank.overlimittransaction.constants;

public enum OrderCategory {
    DOMESTIC,
    FX
}
