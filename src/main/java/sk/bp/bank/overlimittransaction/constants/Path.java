package sk.bp.bank.overlimittransaction.constants;

public class Path {

    public static final String ROOT = "/v1/transaction";

    private Path() {
    }
}
