package sk.bp.bank.overlimittransaction.constants;

public enum State {
    CREATED,
    CANCELLED,
    CLOSED
}
