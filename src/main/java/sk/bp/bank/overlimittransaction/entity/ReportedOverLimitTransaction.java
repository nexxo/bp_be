package sk.bp.bank.overlimittransaction.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import sk.bp.bank.account.entity.Account;
import sk.bp.bank.overlimittransaction.Amount;
import sk.bp.bank.vaultsupply.entity.VaultSupply;
import sk.bp.bank.overlimittransaction.constants.OrderCategory;
import sk.bp.bank.overlimittransaction.constants.State;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "reported_overlimit_transactions")
public class ReportedOverLimitTransaction {

    @Id
    @NotNull
    private String id;

    @NotNull
    private OrderCategory orderCategory;

    @NotNull
    private State state;

    @NotNull
    private Account sourceAccount;

    @NotNull
    private String clientId;

    @NotNull
    private String identificationId;

    @NotNull
    private Amount amount;

    @NotNull
    private List<VaultSupply> vault;

    @NotNull
    @LastModifiedDate
    private Date modificationDate = new Date();

    @NotNull
    private Date transferDate = new Date();

    @Size(max = 500)
    private String note;

    @NotNull
    private String organizationalUnitID;

    @NotNull
    private String createdBy;
}
