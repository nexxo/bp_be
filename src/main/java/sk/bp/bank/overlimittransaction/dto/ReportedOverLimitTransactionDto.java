package sk.bp.bank.overlimittransaction.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import sk.bp.bank.account.dto.AccountDto;
import sk.bp.bank.overlimittransaction.Amount;
import sk.bp.bank.overlimittransaction.constants.OrderCategory;
import sk.bp.bank.overlimittransaction.constants.State;
import sk.bp.bank.overlimittransaction.entity.ReportedOverLimitTransaction;
import sk.bp.bank.vaultsupply.dto.VaultSupplyDto;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class ReportedOverLimitTransactionDto {

    @JsonProperty("id")
    private String id;

    @JsonProperty("orderCategory")
    private OrderCategory orderCategory;

    @JsonProperty("state")
    private State state;

    @JsonProperty("sourceAccount")
    private AccountDto sourceAccount;

    @JsonProperty("clientId")
    private String clientId;

    @JsonProperty("identificationId")
    private String identificationId;

    @JsonProperty("amount")
    private Amount amount;

    @JsonProperty("vault")
    private List<VaultSupplyDto> vault;

    @JsonProperty("modificationDate")
    private Date modificationDate;

    @JsonProperty("transferDate")
    private Date transferDate;

    @JsonProperty("note")
    private String note;

    @JsonProperty("organizationalUnitID")
    private String organizationalUnitID;

    @JsonProperty("createdBy")
    private String createdBy;

    public ReportedOverLimitTransactionDto(ReportedOverLimitTransaction tx) {
        this.id = tx.getId();
        this.orderCategory = tx.getOrderCategory();
        this.state = tx.getState();
        this.sourceAccount = new AccountDto(tx.getSourceAccount());
        this.identificationId = tx.getIdentificationId();
        this.amount = tx.getAmount();
        this.vault = tx.getVault().stream().map(VaultSupplyDto::new).collect(Collectors.toList());
        this.modificationDate = getModificationDate();
        this.transferDate = tx.getTransferDate();
        this.note = tx.getNote();
        this.organizationalUnitID = tx.getOrganizationalUnitID();
        this.createdBy = tx.getCreatedBy();
    }
}
