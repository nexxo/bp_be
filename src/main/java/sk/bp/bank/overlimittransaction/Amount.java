package sk.bp.bank.overlimittransaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Amount {

    private double value;

    private int precision;

    @NotNull
    private String currency;
}
