package sk.bp.bank.overlimittransaction.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;
import sk.bp.bank.overlimittransaction.constants.Path;
import sk.bp.bank.overlimittransaction.entity.ReportedOverLimitTransaction;
import sk.bp.bank.util.exceptions.ContentNotFoundException;
import sk.bp.bank.overlimittransaction.service.ReportedOverLimitTransactionService;
import sk.bp.bank.util.TransactionValidator;

import java.util.List;

import static sk.bp.bank.overlimittransaction.constants.ErrorCode.ID_NOT_FOUND;

@RestController
@AllArgsConstructor
@RequestMapping(Path.ROOT)
@CrossOrigin(origins = {"http://localhost:4200"})
public class ReportedOverLimitTransactionController {

    private final ReportedOverLimitTransactionService transactionService;
    private final TransactionValidator transactionValidator;

    @GetMapping("/{id}")
    public Mono<ResponseEntity<ReportedOverLimitTransaction>> getOneTransaction(@PathVariable String id) {
        return transactionService
                .getOneTransaction(id)
                .map(tx -> ResponseEntity.ok().body(tx))
                .switchIfEmpty(Mono.error(new ContentNotFoundException(ID_NOT_FOUND)));
    }

    @PostMapping
    public Mono<ResponseEntity<ReportedOverLimitTransaction>> createTransaction(@NonNull @RequestBody ReportedOverLimitTransaction transaction) {
        final List<String> errorCodesList = transactionValidator.validateTransaction(transaction);

        if (errorCodesList.isEmpty()) {
            return transactionService
                    .createTransaction(transaction)
                    .map(ResponseEntity::ok);
        }
        return Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, String.join(", ", errorCodesList)));
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> deleteTransaction(@PathVariable String id) {
        return transactionService
                .getOneTransaction(id)
                .switchIfEmpty(Mono.error(new ContentNotFoundException(ID_NOT_FOUND)))
                .then(transactionService.deleteTransactionById(id))
                .flatMap(tx -> Mono.just(ResponseEntity.ok().build()));
    }
}
