package sk.bp.bank.overlimittransaction.service;

import reactor.core.publisher.Mono;
import reactor.util.annotation.NonNull;
import sk.bp.bank.overlimittransaction.entity.ReportedOverLimitTransaction;

public interface ReportedOverLimitTransactionService {
    Mono<ReportedOverLimitTransaction> getOneTransaction(@NonNull String id);
    Mono<Void> deleteTransactionById(@NonNull String id);
    Mono<ReportedOverLimitTransaction> createTransaction(@NonNull ReportedOverLimitTransaction transaction);
}
