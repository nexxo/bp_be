package sk.bp.bank.overlimittransaction.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.annotation.NonNull;
import sk.bp.bank.overlimittransaction.dao.ReportedOverLimitTransactionRepository;
import sk.bp.bank.overlimittransaction.entity.ReportedOverLimitTransaction;
import sk.bp.bank.vaultsupply.entity.VaultSupply;
import sk.bp.bank.vaultsupply.service.VaultSuppliesService;

import java.util.List;

@Service
@AllArgsConstructor
public class ReportedOverLimitTransactionServiceImpl implements ReportedOverLimitTransactionService {

    private final ReportedOverLimitTransactionRepository reportedOverLimitTransactionRepository;
    private final VaultSuppliesService vaultSuppliesService;

    @Override
    public Mono<ReportedOverLimitTransaction> getOneTransaction(@NonNull String id) {
        return reportedOverLimitTransactionRepository.findById(id);
    }

    @Override
    public Mono<Void> deleteTransactionById(@NonNull String id) {
        return reportedOverLimitTransactionRepository.deleteById(id);
    }

    @Override
    public Mono<ReportedOverLimitTransaction> createTransaction(@NonNull ReportedOverLimitTransaction transaction) {
        return reportedOverLimitTransactionRepository.save(transaction);
    }
}
