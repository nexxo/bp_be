package sk.bp.bank.overlimittransaction.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;
import reactor.util.annotation.NonNull;
import sk.bp.bank.overlimittransaction.entity.ReportedOverLimitTransaction;

@Repository
public interface ReportedOverLimitTransactionRepository extends ReactiveMongoRepository<ReportedOverLimitTransaction, String> {
    Mono<Void> deleteById(@NonNull String id);
}
