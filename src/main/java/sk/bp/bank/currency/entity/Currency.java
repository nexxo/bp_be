package sk.bp.bank.currency.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "currencies")
public class Currency {

    @Id
    @NotNull
    private String id;

    private String symbol;

    private String name;

    private int decimalDigits;

    private String code;

    private Boolean available;
}
