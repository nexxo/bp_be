package sk.bp.bank.currency.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import sk.bp.bank.currency.entity.Currency;

@Getter
public class CurrencyDto {

    @JsonProperty("id")
    private String id;

    @JsonProperty("symbol")
    private String symbol;

    @JsonProperty("name")
    private String name;

    @JsonProperty("decimalDigits")
    private int decimalDigits;

    @JsonProperty("code")
    private String code;

    @JsonProperty("isAvailable")
    private boolean isAvailable;

    public CurrencyDto(Currency currency) {
        this.id = currency.getId();
        this.symbol = currency.getSymbol();
        this.name = currency.getName();
        this.decimalDigits = currency.getDecimalDigits();
        this.code = currency.getCode();
        this.isAvailable = currency.getAvailable();
    }
}
