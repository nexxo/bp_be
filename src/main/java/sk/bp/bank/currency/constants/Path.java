package sk.bp.bank.currency.constants;

public class Path {

    public static final String ROOT = "/v1/currency";

    private Path() {
    }
}
