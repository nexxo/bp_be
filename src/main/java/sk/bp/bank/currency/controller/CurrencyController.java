package sk.bp.bank.currency.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import sk.bp.bank.currency.constants.Path;
import sk.bp.bank.currency.dto.CurrencyDto;
import sk.bp.bank.currency.service.CurrencyService;

@RestController
@AllArgsConstructor
@RequestMapping(Path.ROOT)
@CrossOrigin(origins = {"http://localhost:4200"})
public class CurrencyController {

    private final CurrencyService currencyService;

    @GetMapping
    public Flux<CurrencyDto> getAllAvailableCurrencies() {
        return currencyService
                .getAllAvailableCurrencies()
                .map(CurrencyDto::new);
    }
}
