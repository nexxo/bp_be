package sk.bp.bank.currency.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sk.bp.bank.currency.entity.Currency;

@Repository
public interface CurrencyRepository extends ReactiveMongoRepository<Currency, String> {

    Flux<Currency> findAllByAvailableTrue();
    Mono<Boolean> existsByAvailableTrueAndCodeEquals(String code);
}
