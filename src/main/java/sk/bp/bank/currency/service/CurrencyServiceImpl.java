package sk.bp.bank.currency.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sk.bp.bank.currency.dao.CurrencyRepository;
import sk.bp.bank.currency.entity.Currency;

@Service
@AllArgsConstructor
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository currencyRepository;

    public Flux<Currency> getAllAvailableCurrencies() {
        return this.currencyRepository.findAllByAvailableTrue();
    }

    public Mono<Boolean> isAvailableWithCode(String code) {
        return currencyRepository.existsByAvailableTrueAndCodeEquals(code);
    }
}
