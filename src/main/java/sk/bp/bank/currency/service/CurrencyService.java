package sk.bp.bank.currency.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sk.bp.bank.currency.entity.Currency;

public interface CurrencyService {
    Flux<Currency> getAllAvailableCurrencies();
    Mono<Boolean> isAvailableWithCode(String code);
}
