package sk.bp.bank.auth;

public class Path {

    public static final String ROOT = "/v1/auth";
    public static final String LOGIN = "/login";

    private Path() {
    }
}
