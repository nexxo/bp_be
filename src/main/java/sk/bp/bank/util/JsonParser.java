package sk.bp.bank.util;

import com.google.gson.GsonBuilder;
import lombok.NoArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

@Component
@NoArgsConstructor
public class JsonParser<T> {

    public T parse(String fileName, Class<T> clazz) throws IOException {
        try (InputStream is = new ClassPathResource(fileName).getInputStream()) {
            StringWriter writer = new StringWriter();
            IOUtils.copy(is, writer, "UTF-8");
            String json = writer.toString();

            return new GsonBuilder().create().fromJson(json, clazz);
        }
    }
}
