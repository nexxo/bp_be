package sk.bp.bank.util;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import sk.bp.bank.util.dto.GoogleCalendarDto;
import sk.bp.bank.util.dto.GoogleCalendarEntryDto;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static java.util.Calendar.*;

@Service
@AllArgsConstructor
public class GoogleCalendarUtils {

    private final JsonParser<GoogleCalendarDto> parser;

    @Value("${timezone}")
    private String timeZone;

    @Value("${resource.public-holiday}")
    private String publicHolidaysResource;

    @Autowired
    public GoogleCalendarUtils(JsonParser<GoogleCalendarDto> parser) {
        this.parser = parser;
    }

    private GoogleCalendarDto getRawCalendar() throws IOException {
        return parser.parse(publicHolidaysResource, GoogleCalendarDto.class);
    }

    private boolean isPublicHoliday(Date date) {
        try {
            return getRawCalendar()
                    .getItems()
                    .stream()
                    .anyMatch(calendarEntry -> isPublicHoliday(calendarEntry, date));
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isBankingDay(Date date) {
        return !isPublicHoliday(date) && !isWeekend(date);
    }

    private boolean isPublicHoliday(GoogleCalendarEntryDto calendarEntry, Date date) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final Calendar calendar = getInstance(TimeZone.getTimeZone(timeZone));

        calendar.setTime(date);
        calendar.set(YEAR, 2018); // TODO google calendar retrieved holidays only for 2018

        try {
            final Date holidayStartDate = dateFormat.parse(calendarEntry.getStart().getDate()); // 2018
            final Date holidayEndDate = dateFormat.parse(calendarEntry.getEnd().getDate()); // 2018
            final Date converted = calendar.getTime();

            return converted.after(holidayStartDate) && converted.before(holidayEndDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean isWeekend(Date date) {
        Calendar calendar = getInstance(TimeZone.getTimeZone(timeZone));
        calendar.setTime(date);
        final int day = calendar.get(DAY_OF_WEEK);

        return day == SATURDAY || day == SUNDAY;
    }
}
