package sk.bp.bank.util.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserDto {

    private String email;

    private String displayName;

    private boolean self;
}
