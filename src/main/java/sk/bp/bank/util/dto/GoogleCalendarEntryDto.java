package sk.bp.bank.util.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.Date;

@Getter
@AllArgsConstructor
@ToString
public class GoogleCalendarEntryDto {

    private String kind;

    private String etag;

    private String id;

    private String status;

    private String htmlLink;

    private Date created;

    private Date updated;

    private String summary;

    private UserDto creator;

    private UserDto organizer;

    private CalendarEntryDateDto start;

    private CalendarEntryDateDto end;

    private String transparency;

    private String visibility;

    private String isCalUID;

    private Long sequence;
}
