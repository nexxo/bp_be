package sk.bp.bank.util.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Getter
public class GoogleCalendarDto {

    private String kind;

    private String etag;

    private String summary;

    private Date updated;

    private String timeZone;

    private String accessRole;

    private List<String> defaultReminders;

    private String nextSyncToken;

    private List<GoogleCalendarEntryDto> items;
}
