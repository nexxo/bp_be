package sk.bp.bank.util.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class CalendarEntryDateDto {

    private String date;
}
