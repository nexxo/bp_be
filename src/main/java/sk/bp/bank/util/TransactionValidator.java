package sk.bp.bank.util;

import lombok.AllArgsConstructor;
import org.apache.commons.validator.routines.IBANValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.annotation.NonNull;
import sk.bp.bank.account.entity.Account;
import sk.bp.bank.currency.service.CurrencyService;
import sk.bp.bank.overlimittransaction.Amount;
import sk.bp.bank.overlimittransaction.constants.OrderCategory;
import sk.bp.bank.overlimittransaction.entity.ReportedOverLimitTransaction;

import java.util.*;

import static java.util.Calendar.*;
import static sk.bp.bank.overlimittransaction.constants.ErrorCode.*;
import static sk.bp.bank.overlimittransaction.constants.OrderCategory.*;

@Component
@AllArgsConstructor
public class TransactionValidator {

    private static final double AMOUNT_LIMIT = 999999999.99;
    private static final int MAX_FUTURE_TRANSACTION_DAYS = 3;

    @Value("${timezone}")
    private String timeZone;
    private final GoogleCalendarUtils googleCalendarUtils;
    private final IBANValidator ibanValidator;
    private final CurrencyService currencyService;

    @Autowired
    public TransactionValidator(GoogleCalendarUtils googleCalendarUtils,  CurrencyService currencyService) {
        this.googleCalendarUtils = googleCalendarUtils;
        this.ibanValidator = IBANValidator.getInstance();
        this.currencyService = currencyService;
    }

    public List<String> validateTransaction(@NonNull ReportedOverLimitTransaction tx) {
        final List<String> collectedDiscrepancies = new ArrayList<>();
        final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(timeZone));

        final Account sourceAccount = tx.getSourceAccount();
        final Amount amount = tx.getAmount();
        final OrderCategory orderCategory = tx.getOrderCategory();
        final String currencyCode = tx.getAmount().getCurrency();
        final Date transferDate = tx.getTransferDate();

        // validate currency
        final Mono<Boolean> isPresent = currencyService.isAvailableWithCode(tx.getAmount().getCurrency());
        final Optional<Boolean> resultOptional = isPresent.blockOptional();

        if (resultOptional.isPresent() && !resultOptional.get()) {
            collectedDiscrepancies.add(CURRENCY_NOT_AVAILABLE);
        }

        // validate dates
        calendar.add(DATE, MAX_FUTURE_TRANSACTION_DAYS);
        if (transferDate.before(calendar.getTime())) {
            collectedDiscrepancies.add(FIELD_INVALID_TOO_NEAR_IN_FUTURE);
        }

        if (transferDate.before(getInstance(TimeZone.getTimeZone(timeZone)).getTime())) {
            collectedDiscrepancies.add(INVALID_DATE_IN_PAST);
        }

        if (!googleCalendarUtils.isBankingDay(transferDate)) {
            collectedDiscrepancies.add(INVALID_DATE);
        }

        // validate iban
        if (!ibanValidator.isValid(tx.getSourceAccount().getIban())) {
            collectedDiscrepancies.add(IBAN_INVALID);
        }

        // validate limits
        if (sourceAccount.getCurrentBalance() + sourceAccount.getOverLimitValue() - amount.getValue() < 0) {
            collectedDiscrepancies.add(LIMIT_EXCEEDED);
        }

        if (amount.getValue() <= 0 || amount.getValue() > AMOUNT_LIMIT) {
            collectedDiscrepancies.add(FIELD_INVALID);
        }

        // validate category
        if (orderCategory.equals(DOMESTIC) && !currencyCode.equals("EUR")) {
            collectedDiscrepancies.add(CATEGORY_INVALID);
        }

        else if (orderCategory.equals(FX) && currencyCode.equals("EUR")) {
            collectedDiscrepancies.add(CATEGORY_INVALID);
        }

        return collectedDiscrepancies;
    }

}
