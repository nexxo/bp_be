package sk.bp.bank.util.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static sk.bp.bank.overlimittransaction.constants.ErrorCode.ID_NOT_FOUND;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = ID_NOT_FOUND)
public class ContentNotFoundException extends RuntimeException {
    public ContentNotFoundException(String message) {
        super(message);
    }
}
