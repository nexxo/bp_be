package sk.bp.bank.account.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {

    @Id
    private String id;

    @Pattern(regexp = "[A-Z]{2}[0-9]{2}[0-9a-zA-Z]{1,30}")
    private String iban;

    private String productDescription;

    private double currentBalance;

    private double overLimitValue;

    private String name;

    private String note;
}
