package sk.bp.bank.account.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import sk.bp.bank.account.entity.Account;

@Getter
public class AccountDto {

    @JsonProperty("id")
    private String id;

    @JsonProperty("iban")
    private String iban;

    @JsonProperty("currentBalance")
    private double currentBalance;

    @JsonProperty("note")
    private String note;

    @JsonProperty("name")
    private String name;

    @JsonProperty("overLimitValue")
    private double overLimitValue;

    @JsonProperty("productDescription")
    private String productDescription;

    public AccountDto(Account account) {
        this.currentBalance = account.getCurrentBalance();
        this.iban = account.getIban();
        this.note = account.getNote();
        this.overLimitValue = account.getOverLimitValue();
        this.name = account.getName();
        this.productDescription = account.getProductDescription();
        this.id = account.getId();
    }
}
