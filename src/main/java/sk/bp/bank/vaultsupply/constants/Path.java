package sk.bp.bank.vaultsupply.constants;

public class Path {

    public static final String ROOT = "/v1/vault-supplies";

    // disable instantiation of this class
    private Path() {
    }
}
