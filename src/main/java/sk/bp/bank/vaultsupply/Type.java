package sk.bp.bank.vaultsupply;

public enum Type {
    BANKNOTE,
    COIN
}
