package sk.bp.bank.vaultsupply.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import sk.bp.bank.vaultsupply.Type;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@Document(collection = "vault_supplies")
@AllArgsConstructor
public class VaultSupply {

    @Id
    @NotNull
    private String id;

    private double nominalValue;

    private int number;

    @NotNull
    private Type type;
}
