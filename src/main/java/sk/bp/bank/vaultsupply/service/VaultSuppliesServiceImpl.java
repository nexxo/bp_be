package sk.bp.bank.vaultsupply.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import sk.bp.bank.vaultsupply.dao.VaultSuppliesRepository;
import sk.bp.bank.vaultsupply.entity.VaultSupply;

import java.util.List;

@Service
@AllArgsConstructor
public class VaultSuppliesServiceImpl implements VaultSuppliesService {

    private final VaultSuppliesRepository vaultSuppliesRepository;

    @Override
    public Flux<VaultSupply> getAllVaultSupplies() {
        return vaultSuppliesRepository.findAll();
    }

    @Override
    public Flux<VaultSupply> updateAllVaultSupplies(Flux<VaultSupply> vaultSupplies) {
        return vaultSuppliesRepository.saveAll(vaultSupplies);
    }
}
