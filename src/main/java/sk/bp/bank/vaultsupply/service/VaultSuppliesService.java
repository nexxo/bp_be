package sk.bp.bank.vaultsupply.service;

import reactor.core.publisher.Flux;
import sk.bp.bank.vaultsupply.entity.VaultSupply;

public interface VaultSuppliesService {

    Flux<VaultSupply> getAllVaultSupplies();
    Flux<VaultSupply> updateAllVaultSupplies(Flux<VaultSupply> vaultSupplies);
}
