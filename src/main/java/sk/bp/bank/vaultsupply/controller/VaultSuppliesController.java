package sk.bp.bank.vaultsupply.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import sk.bp.bank.vaultsupply.constants.Path;
import sk.bp.bank.vaultsupply.dto.VaultSupplyDto;
import sk.bp.bank.vaultsupply.entity.VaultSupply;
import sk.bp.bank.vaultsupply.service.VaultSuppliesService;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping(Path.ROOT)
@AllArgsConstructor
public class VaultSuppliesController {

    private final VaultSuppliesService vaultSuppliesService;

    @PutMapping
    public Flux<VaultSupplyDto> updateAllUsedVaultSupplies(@RequestBody Flux<VaultSupply> vaultSupplies) {
        return vaultSuppliesService
                .updateAllVaultSupplies(vaultSupplies   )
                .map(VaultSupplyDto::new);
    }

    @GetMapping
    public Flux<VaultSupplyDto> getAllVaultSupplies() {
        return vaultSuppliesService
                .getAllVaultSupplies()
                .map(VaultSupplyDto::new);
    }
}
