package sk.bp.bank.vaultsupply.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import sk.bp.bank.vaultsupply.entity.VaultSupply;

import java.util.List;


@Repository
public interface VaultSuppliesRepository extends ReactiveMongoRepository<VaultSupply, String> {
    Flux<VaultSupply> saveAll(List<VaultSupply> vaultSupplies);
}
