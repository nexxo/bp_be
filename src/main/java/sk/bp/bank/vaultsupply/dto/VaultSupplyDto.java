package sk.bp.bank.vaultsupply.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import sk.bp.bank.vaultsupply.Type;
import sk.bp.bank.vaultsupply.entity.VaultSupply;

@Getter
public class VaultSupplyDto {

    @JsonProperty("id")
    private String id;

    @JsonProperty("nominalValue")
    private double nominalValue;

    @JsonProperty("number")
    private int number;

    @JsonProperty("type")
    private Type type;

    public VaultSupplyDto(VaultSupply vaultSupply) {
        this.id = vaultSupply.getId();
        this.nominalValue = vaultSupply.getNominalValue();
        this.number = vaultSupply.getNumber();
        this.type = vaultSupply.getType();
    }
}
