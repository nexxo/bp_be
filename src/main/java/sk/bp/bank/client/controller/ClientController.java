package sk.bp.bank.client.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sk.bp.bank.client.constants.Path;
import sk.bp.bank.client.dto.ClientDto;
import sk.bp.bank.client.service.ClientService;


@RestController
@RequestMapping(Path.ROOT)
@AllArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class ClientController {

    private final ClientService clientService;

    @GetMapping("/{identificationId}")
    public Flux<ClientDto> getClientByIdentificationIdRegex(@PathVariable String identificationId) {
        return clientService
                .getClientByIdentificationIdRegex("^" + identificationId)
                .map(ClientDto::new);
    }

    @GetMapping
    public Mono<ResponseEntity<ClientDto>> getClientByIdentificationId(@RequestParam String identificationId) {
        return clientService
                .getClientByIdentificationId(identificationId)
                .map(ClientDto::new)
                .flatMap(clientDto -> Mono.just(ResponseEntity.ok().body(clientDto)))
                .switchIfEmpty(Mono.just(ResponseEntity.noContent().build()));
    }
}
