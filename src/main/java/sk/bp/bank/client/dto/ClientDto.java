package sk.bp.bank.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import sk.bp.bank.account.dto.AccountDto;
import sk.bp.bank.client.entity.Client;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class ClientDto {

    @JsonProperty("id")
    private String id;

    @JsonProperty("forename")
    private String forename;

    @JsonProperty("surname")
    private String surname;

    @JsonProperty("birth")
    private Date birth;

    @JsonProperty("identificationId")
    private String identificationId;

    @JsonProperty("accounts")
    private List<AccountDto> accounts;

    public ClientDto(Client client) {
        this.id = client.getId();
        this.forename = client.getForename();
        this.surname = client.getSurname();
        this.birth = client.getBirth();
        this.identificationId = client.getIdentificationId();
        this.accounts = client
                .getAccounts()
                .stream()
                .map(AccountDto::new)
                .collect(Collectors.toList());
    }
}
