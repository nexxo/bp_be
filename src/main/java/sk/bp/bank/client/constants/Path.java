package sk.bp.bank.client.constants;

public class Path {

    public static final String ROOT = "/v1/client";

    private Path() {
    }
}
