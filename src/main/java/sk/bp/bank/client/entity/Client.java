package sk.bp.bank.client.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import sk.bp.bank.account.entity.Account;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Document(collection = "clients")
@AllArgsConstructor
@NoArgsConstructor
public class Client {

    @Id
    private String id;

    @NotNull
    private String forename;

    @NotNull
    private String surname;

    @NotNull
    private Date birth;

    @NotNull
    private String identificationId;

    private List<Account> accounts = new ArrayList<>();
}
