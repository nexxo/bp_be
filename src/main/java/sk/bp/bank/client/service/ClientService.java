package sk.bp.bank.client.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sk.bp.bank.client.entity.Client;

public interface ClientService {
    Flux<Client> getClientByIdentificationIdRegex(String identificationId);
    Mono<Client> getClientByIdentificationId(String identificationId);
}
