package sk.bp.bank.client.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sk.bp.bank.client.dao.ClientRepository;
import sk.bp.bank.client.entity.Client;

@Service
@AllArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    @Override
    public Flux<Client> getClientByIdentificationIdRegex(String identificationId) {
        return clientRepository.findClientByIdentificationIdRegex(identificationId);
    }

    @Override
    public Mono<Client> getClientByIdentificationId(String identificationId) {
        return clientRepository.findClientByIdentificationId(identificationId);
    }
}
