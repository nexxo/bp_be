package sk.bp.bank.client.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sk.bp.bank.client.entity.Client;

@Repository
public interface ClientRepository extends ReactiveMongoRepository<Client, String> {

    Flux<Client> findClientByIdentificationIdRegex(String identificationId);
    Mono<Client> findClientByIdentificationId(String identificationId);
}
